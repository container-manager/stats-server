module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "no-underscore-dangle": "off",
    "no-restricted-syntax": "off",
    "no-await-in-loop": "off",
    "global-require": "off",
    "import/no-dynamic-require": "off",
    "no-continue": "off",
    "guard-for-in": "off",
    "no-console": "off",
  },
  "plugins": [
    "jest"
  ],
  "env": {
    "jest": true
  }
};
