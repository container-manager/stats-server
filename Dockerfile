FROM node:10-alpine AS dependencies

RUN mkdir /app
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --only=prod

FROM dependencies as src
COPY . .

FROM src AS test
RUN npm install && npm run-script lint && npm run-script test

FROM src AS release
EXPOSE 8080
VOLUME /data
ENV STATS_STATE_DIR=/data
CMD npm start
