const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const WebSocket = require('ws');
// const fs = require('fs').promises;
const http = require('http');

const StatsManager = require('./lib/StatsManager');

const server = http.createServer();
const wss = new WebSocket.Server({ server });

const app = express();
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(express.static('public'));

const statsManager = new StatsManager();

// write state after every request
app.use(async (req, res, next) => {
  await next();
  if (res.statusCode === 200 && req.method === 'POST') {
    await statsManager.writeState();
  }
});

// dump body on exception
app.use((error, req, res, next) => {
  if (error) {
    console.log(error);
    console.log(JSON.stringify(req.body));
    res.status(400).send(error.message);
  }
  next();
});

server.on('request', app);

wss.on('connection', (client) => {
  const { state } = statsManager;
  for (const host of Object.values(state)) {
    client.send(JSON.stringify(host));
  }
});

statsManager.on('broadcast', (msg) => {
  const str = JSON.stringify(msg);
  wss.clients.forEach((client) => {
    client.send(str);
  });
});

app.post('/init', (req, res) => {
  const { body } = req;
  statsManager.handleInit(body);
  res.sendStatus(200);
});

app.post('/stat', (req, res) => {
  const { body } = req;
  statsManager.handleStat(body);
  res.sendStatus(200);
});

app.post('/shutdown', (req, res) => {
  const { body } = req;
  statsManager.handleShutdown(body);
  res.sendStatus(200);
});

app.get('/info', (req, res) => {
  res.send(statsManager.state);
});

(async () => {
  if (process.env.TRUST_PROXY) {
    app.set('trust proxy', true);
  }
  const port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
  const ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
  await statsManager.loadState();
  server.listen(port, ip, () => { console.log(`Listening on ${port}`); });
})();
