const EventEmitter = require('events');
const fs = require('fs').promises;
const process = require('process');
const path = require('path');

class StatsManager extends EventEmitter {
  constructor() {
    super();
    this.state = {};
    let stateDir = process.env.STATS_STATE_DIR;
    if (!stateDir) {
      stateDir = '/tmp';
    }
    this.statePath = path.join(stateDir, 'state.json');
  }

  async writeState() {
    const str = JSON.stringify(this.state);
    await fs.writeFile(this.statePath, str);
  }

  async loadState() {
    try {
      const str = await fs.readFile(this.statePath);
      this.state = JSON.parse(str);
    } catch (e) {
      console.log(`Unable to loadState: ${e}`);
    }
  }

  handleInit(data) {
    const { hostname, info } = data;
    if (!hostname) {
      throw new Error('hostname is a required parameter');
    }
    if (!info) {
      throw new Error('info is a required parameter');
    }

    const stats = {
      cpu: 0,
      disk: 0,
      gpu: {},
    };

    for (const idx in info.gpu) {
      stats.gpu[idx] = {};
    }
    const obj = {
      hostname,
      info,
      stats,
    };

    this.emit('broadcast', obj);
    this.state[hostname] = obj;
  }

  handleStat(data) {
    const { hostname, stats } = data;
    if (!hostname) {
      throw new Error('hostname is a required parameter');
    }
    if (!stats) {
      throw new Error('stats is a required parameter');
    }

    const hostObj = this.state[hostname];
    if (!hostObj) {
      throw new Error('stats message for uninitialized host');
    }

    const hostStats = hostObj.stats;
    const now = Date.now();

    for (const key in stats) {
      const val = stats[key];
      if (typeof (val) === 'object') {
        // inject timestamp into each object
        // eslint-disable-next-line no-param-reassign
        Object.values(val).forEach((a) => { a.timestamp = now; });

        Object.assign(hostStats[key], val);
      } else {
        hostStats[key] = val;
      }
    }
    this.emit('broadcast', data);
  }

  handleShutdown(data) {
    const { hostname } = data;
    if (!hostname) {
      throw new Error('hostname is a required parameter');
    }
    if (!(hostname in this.state)) {
      throw new Error('shutdown for uninitialized host');
    }

    delete this.state[hostname];
    this.emit('broadcast', { hostname });
  }
}

module.exports = StatsManager;
