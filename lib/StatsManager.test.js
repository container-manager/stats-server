const StatsManager = require('./StatsManager');

const initMsg = {
  hostname: 'test',
  info: {
    gpu: {
      0: {},
      1: {},
    },
  },
};

const getGpuMsg0 = {
  hostname: 'test',
  stats: {
    gpu: {
      0: { container: 'my-container', user: 'my-user' },
    },
  },
};

const getGpuMsg1 = {
  hostname: 'test',
  stats: {
    gpu: {
      1: { container: 'my-container', user: 'my-user' },
    },
  },
};

const releaseGpuMsg0 = {
  hostname: 'test',
  stats: {
    gpu: {
      0: {},
    },
  },
};

test('init', () => {
  const m = new StatsManager();
  m.handleInit(initMsg);
});

test('get one gpu', () => {
  const m = new StatsManager();
  m.handleInit(initMsg);
  m.handleStat(getGpuMsg0);
  expect(m.state).toMatchObject({
    test: getGpuMsg0,
  });
});

test('get one, then release gpu', () => {
  const m = new StatsManager();
  m.handleInit(initMsg);
  m.handleStat(getGpuMsg0);
  m.handleStat(releaseGpuMsg0);
  expect(m.state).toMatchObject({
    test: releaseGpuMsg0,
  });
});

test('get two, then release one gpu', () => {
  const m = new StatsManager();
  m.handleInit(initMsg);
  m.handleStat(getGpuMsg0);
  m.handleStat(getGpuMsg1);
  m.handleStat(releaseGpuMsg0);

  expect(m.state).toMatchObject({
    test: releaseGpuMsg0,
  });

  expect(m.state).toMatchObject({
    test: getGpuMsg1,
  });
});

test('state rw', async () => {
  const m = new StatsManager();
  m.handleInit(initMsg);
  m.handleStat(getGpuMsg0);
  m.handleStat(getGpuMsg1);
  m.handleStat(releaseGpuMsg0);

  await m.writeState();

  const m2 = new StatsManager();
  await m2.loadState();

  expect(m.state).toMatchObject(m2.state);
});
