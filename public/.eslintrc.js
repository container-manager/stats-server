module.exports = {
    "extends": "airbnb-base",
    "env": {
        "browser": true,
    },
    "rules": {
        "guard-for-in": "off",
        "no-restricted-syntax": "off",
        "no-param-reassign": "off",
        "max-len": "off",
        "no-plusplus": "off",
    }
};