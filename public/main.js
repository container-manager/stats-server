function createHostRow(hostname, number) {
  const container = document.createElement('div');
  container.className = 'host-container';
  container.id = `${hostname}-container`;
  container.gpus = number;

  const title = document.createElement('h2');
  title.innerText = hostname;
  container.appendChild(title);
  const message = document.createElement('p');
  message.className = 'message';
  container.appendChild(message);

  const statusRow = document.createElement('div');
  statusRow.className = 'status-row';

  for (let i = 0; i < number; ++i) {
    const gpu = document.createElement('div');
    gpu.className = 'gpu';
    gpu.id = `${hostname}-gpu-${i}`;
    gpu.innerText = i;

    const lockedIndicator = document.createElement('span');
    lockedIndicator.className = 'lock-indicator';
    gpu.appendChild(lockedIndicator);

    const tooltip = document.createElement('div');

    const userTitle = document.createElement('span');
    userTitle.innerText = 'Username';
    userTitle.className = 'tooltip-title';
    tooltip.appendChild(userTitle);
    const userContent = document.createElement('span');
    userContent.className = 'tooltip-username';
    tooltip.appendChild(userContent);

    const containerTitle = document.createElement('span');
    containerTitle.innerText = 'Container';
    containerTitle.className = 'tooltip-title';
    tooltip.appendChild(containerTitle);
    const containerContent = document.createElement('span');
    containerContent.className = 'tooltip-container';
    tooltip.appendChild(containerContent);

    const sinceTitle = document.createElement('span');
    sinceTitle.innerText = 'Since';
    sinceTitle.className = 'tooltip-title';
    tooltip.appendChild(sinceTitle);
    const sinceContent = document.createElement('span');
    sinceContent.className = 'tooltip-since';
    tooltip.appendChild(sinceContent);

    gpu.appendChild(tooltip);
    statusRow.appendChild(gpu);
  }
  if (number > 0) {
    container.appendChild(statusRow);
  }

  const progressContainer = document.createElement('div');
  progressContainer.className = 'progress-container';

  const cpuContainer = document.createElement('div');
  const cpuLabel = document.createElement('p');
  cpuLabel.className = 'progress-label';
  cpuLabel.innerText = 'CPU%';
  const cpu = document.createElement('progress');
  cpu.className = 'cpu';
  cpu.max = 100;
  cpu.value = 0;
  cpuContainer.appendChild(cpuLabel);
  cpuContainer.appendChild(cpu);
  progressContainer.appendChild(cpuContainer);

  const diskContainer = document.createElement('div');
  const diskLabel = document.createElement('p');
  diskLabel.className = 'progress-label';
  diskLabel.innerText = 'Disk%';
  const disk = document.createElement('progress');
  disk.className = 'disk';
  disk.max = 100;
  disk.value = 0;
  diskContainer.appendChild(diskLabel);
  diskContainer.appendChild(disk);
  progressContainer.appendChild(diskContainer);


  const memoryContainer = document.createElement('div');
  const memoryLabel = document.createElement('p');
  memoryLabel.className = 'progress-label';
  memoryLabel.innerText = 'Mem%';
  const memory = document.createElement('progress');
  memory.className = 'memory';
  memory.max = 100;
  memory.value = 0;
  memoryContainer.appendChild(memoryLabel);
  memoryContainer.appendChild(memory);
  progressContainer.appendChild(memoryContainer);

  container.appendChild(progressContainer);

  return container;
}

function padTwoDigit(number) {
  if (number < 9) {
    return `0${number}`;
  }
  return number;
}
function formatDate(date) {
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = padTwoDigit(date.getHours());
  const minutes = padTwoDigit(date.getMinutes());

  return `${month}/${day} ${hour}:${minutes}`;
}

function processStats(hostname, stats) {
  const row = document.getElementById(`${hostname}-container`);

  const {
    cpu, disk, memory, gpu, message,
  } = stats;

  if (message !== undefined) {
    row.querySelector('.message').innerText = message;
  }
  if (cpu) {
    row.querySelector('.cpu').value = cpu;
  }
  if (disk) {
    row.querySelector('.disk').value = disk;
  }
  if (memory) {
    row.querySelector('.memory').value = memory;
  }
  if (gpu) {
    for (const idx in gpu) {
      const val = gpu[idx];
      const el = document.getElementById(`${hostname}-gpu-${idx}`);
      if (val.container) {
        el.classList.add('gpu-active');
        el.querySelector('.tooltip-username').innerText = val.username;
        el.querySelector('.tooltip-container').innerText = val.container;
        const date = new Date(val.timestamp);
        el.querySelector('.tooltip-since').innerText = formatDate(date);
      } else {
        el.classList.remove('gpu-active');
      }
      if (val.lock) {
        el.classList.add('locked-gpu');
      } else {
        el.classList.remove('locked-gpu');
      }
    }
  }
}

// works for both http -> ws and https -> wss
const wsPath = window.location.href.replace('http', 'ws');
const ws = new WebSocket(wsPath);
const container = document.getElementById('status-container');

ws.onmessage = (msg) => {
  const obj = JSON.parse(msg.data);
  const { hostname, info, stats } = obj;
  if (info) {
    const gpuCount = Object.keys(info.gpu).length;
    const row = createHostRow(hostname, gpuCount);
    container.appendChild(row);
  }
  if (stats) {
    processStats(hostname, stats);
  }
  if (!info && !stats) {
    const row = document.getElementById(`${hostname}-container`);
    row.parentElement.removeChild(row);
  }
};
